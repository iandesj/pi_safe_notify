import pymongo
from pymongo import MongoClient

# Depending on the client, whether local or remote this needs to be configured.
CLIENT = MongoClient() # Default is configured for localhost at port 27017
DB = CLIENT['dev-log-database']
EVENTS = DB['dev-event-collection']
VIDEOS = DB['dev-video-collection']


