# File which holds functions to upload the text logs and door open/close events


def find(document, type):
	# This function will allow for searching for an existing log, which could then be manipulated elsewhere
	if type == events:
		log_doc = EVENTS.find_one({"safe_key":"{key}".format(key=SAFE_KEY), "log_file":"{doc}".format(doc=document)})
		return log_doc
	else:
		return None

def new(document, type):
	# This function will create a new document which will hold event times of door open/close 
	## on mongo db collection
	if type == events:
		log_doc = ({"safe_key":"{key}".format(key=SAFE_KEY), "log_file":"{doc}".format(doc=document)})
		EVENTS.insert(log_doc)
		return log_doc
	else:
		return False

def update(document, type):
	# This function will update the actual document which already exists in the mongo db collection
	

def delete(document, type):
	# This function will delete the actual document which holds log events in mongo db collection