from DBConnect import *

# Setting cursor to the data
cur = DATABASE.cursor()

# Executing SQL statement with the cursor pointed to this specific safes (vault in database) information.
cur.execute("SELECT user_id FROM vaults WHERE safe_key = '5423DMX$'")

# Fetch rows tuple from database for this safe
rows = cur.fetchall()

# Writes user_id from tuple as a string with proper formatting, to local submodule variable user
user = str(rows[0][0]).strip('L')

# Method to get contact emails where contacts reflect the safe owners user id
def get_contact_emails():
	with DATABASE:
		cur.execute("SELECT email FROM contacts WHERE user_id='{userid}'".format(userid=user))

		email_list = cur.fetchall()
		# This loops through the fetched tuples
		# 	and formats each element to a clean looking string then appends it to the EMAILS array
		for email in email_list:
			email = str(email).split(',')[0].strip().strip("()").strip("'")
			EMAILS.append(email)

	return(EMAILS) # Return all contacts emails in an array

# Method to get contact phone numbers where contacts reflect the safe owners user id
def get_contact_phones():
	with DATABASE:
		cur.execute("SELECT phone FROM contacts WHERE user_id = '{userid}'".format(userid=user))

		phones = cur.fetchall()
		# This loops through the fetched tuples
		# 	and formats each element to a clean looking string then appends it to the NUMBERS array
		for phone in phones:
			phone = str(phone).split(',')[0].strip().strip("()").strip("''")
			NUMBERS.append(phone)

	return(NUMBERS) # Return all contacts phone numbers in an array
