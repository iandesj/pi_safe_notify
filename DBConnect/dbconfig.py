import MySQLdb as db

# This database connection information can and should be configured to the applications specs.
DATABASE = db.connect(host="10.0.0.8",user="pi",passwd="safenotify",db="safenotify_dev", port=3306)
EMAILS = [] # Emails array
NUMBERS = [] # Phone numbers array
