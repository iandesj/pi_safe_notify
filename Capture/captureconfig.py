import picamera

# Configuration file to hold all PiCamera settings for video and images
camera = picamera.PiCamera()
camera.ISO = 800
camera.brightness = 55
camera.resolution = (720,480)

# Settings below allow the camera to record upside down
camera.hflip = True
camera.vflip = True
