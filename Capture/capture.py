from Capture import *
import time
import subprocess

# Generic method, returns time stamp used for file indication (date and time)
def capture_stamp(type):
	import os
	
	if type == 'video':
		path, dirs, files = os.walk("/home/pi/Desktop/pi_safe_notify/Videos/5423DMX$/").next()
		file_count = len(files)
		return file_count
	elif type == 'image':
		path, dirs, files = os.walk("/home/pi/Desktop/pi_safe_notify/Images/5423DMX$/").next()
		file_count = len(files)
		return file_count
	# return time.strftime("%m_%d_%Y-%I_%M_%S")


# Captures a single image and stores it in the module directory, can be configured.
def capture_img():
	stamp = capture_stamp('image')
	photo = '/home/pi/Desktop/pi_safe_notify/Images/5423DMX$/module_image{mark}.jpg'.format(mark=stamp) # Image name, saves with timestamp
	camera.capture(photo)

# Starts video recording, with no set ending time
def video_start():
	stamp = capture_stamp('video') # Calling of the timestamp
	videoname = '/home/pi/Desktop/pi_safe_notify/Videos/5423DMX$/module_video{mark}.h264'.format(mark=stamp) # Video name, saves with timestamp
	camera.start_recording(videoname, format='h264') # Initiate recording
	return videoname # Returns videoname, for the sole purpose of it to be passed into the video_stop() method

# Stops video recording that was started, with parameters to take in the file name of the recording video
def video_stop(filename):
	camera.stop_recording()
	# This subprocess, allows commandline program and it's arguments to be called.
	# We're using a program called 'gpac' to convert the raw .h264 video to a readible .mp4 video.
	# The subprocess.call below is how we instantiate this program to do these instructions
	subprocess.call(["MP4Box", "-fps", "30", "-add", 
								filename,
								"{video}.mp4".format(video=filename)
								])
	subprocess.call(["rm", filename])

# Delete raw video capture.
# Captures a video recording with a pre-defined start and stop time of 'length'
# This variable length holds the video length in seconds, default is 200 seconds (for now)
def capture_video():
	length = 200
	stamp = capture_stamp('video')
	video = '/home/pi/Desktop/pi_safe_notify/Videos/5423DMX$/module_video{mark}.h264'.format(mark=stamp)
	camera.start_recording(video, format='h264')
	time.sleep(length)
	camera.stop_recording()
	subprocess.call(["MP4Box", "-fps", "30", "-add", 
								video,
								"{filename}.mp4".format(filename=video)
								])
