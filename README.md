PiSafe (not in development anymore)
==============

An easy to use, easy to configure Python module for the Safe Notify system.

Written using Python 2.7.5

### Dependencies (Python)
- MySQLdb
- pymongo
- googlevoice
- picamera
