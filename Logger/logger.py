from Logger import *

# This method simply appends the the log file and writes to it the time of opening
def log_time_open():
	f = open(LOG_FILE, 'a') # 'a' appends the file for write
	opened = time()
	f.write('Safe door was opened {logtime}\n'.format(logtime=opened))
	f.close()

# This method is just appends the log file and writes to the end the time of closing
def log_time_close():
	f = open(LOG_FILE, 'a') # 'a' appends the file for write
	closed = time()
	f.write('Safe door was closed {logtime}\n\n'.format(logtime=closed))
	f.close()