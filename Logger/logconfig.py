import logging
import datetime

# time() Method is more-less a global method, which returns the current time, used for logging
def time():
	BREACHED_TIME = str(datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"))
	return BREACHED_TIME

LOG_FILE = 'breached.log'