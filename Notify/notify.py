from Notify import *
import time

# Simple method to send texts via the Google Voice package
# This is likely to be changed to a more substantial text service
# Idealy, takes in array of numbers
def send_texts(phoneNumbers):
	voice.login(USERNAME, PASSWORD)

	try:
		for x in phoneNumbers:
			voice.send_sms(x,TEXT)
			time.sleep(6)
			return True
	except:
		print 'Text notifications could not be sent'

# Method to send emails via smtp, currently configured with Gmail
# Idealy, takes in array of email values
def send_emails(emails):
	# Email values are joined to a string, to sendmail() can take them all in
	emails = (', ').join(emails)
	server = smtplib.SMTP('smtp.gmail.com:587')
	msg['Subject'] = 'SafeNotify Safe Alert'
	msg['From'] = FROM
	msg['To'] = emails
	
	server.starttls()
	server.ehlo()
	# Login to server specified
	server.login(USERNAME,PASSWORD)

	try: # try sending mail, may need to be configured
		server.sendmail(FROM, emails, msg.as_string())
		server.quit()
		return True
	except:
		print 'Email notifications could not be sent'

# PROGRAMMER NOTES:

#	considering there may be illegal emails and phone nums that won't work
# 	the send_mail() and send_texts() function's should be refactored more