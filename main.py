import RPi.GPIO as GPIO
from time import sleep
import Logger.logger as l
import Notify.notify as n
import DBConnect.contacts as d
import Capture.capture as c

video_on = False
open = False
checker = True
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while checker == True:
    try:
    	
        print "waiting for rising edge"
        GPIO.wait_for_edge(23, GPIO.RISING)
        print "Safe Opened!"
        open = True
        l.log_time_open()
        emails = d.get_contact_emails()
        phones = d.get_contact_phones()
        n.send_texts(phones)
        n.send_emails(emails)
        vfile = c.video_start()	
        video_on = True
        sleep(30)
        if video_on == True:
        	c.video_stop(vfile)
        	video_on = False
        else:
        	video_on = False	
        
        print "waiting for falling edge"
        GPIO.wait_for_edge(23, GPIO.FALLING)
        print "Safe Closed!"
        open = False
        # rec_time = 0
        l.log_time_close()
        try:
        	c.video_stop(vfile)
        	video_on = False
        except:
        	print "Video is done"
        
    except KeyboardInterrupt:
            GPIO.cleanup()

GPIO.cleanup()
